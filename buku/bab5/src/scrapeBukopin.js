'use strict';


var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));
var cheerio = require('cheerio');
var _ = require('lodash');

var bukopinPromotionBaseUrl = 'https://www.instagram.com/kartukreditbukopin/';
var requestTimeout = 60000;

/**
 * This scraper only scrape the last 12 promotions because of instagram use
 * javascript framework (react).
 * It's better if we scrape the site more often.
 */

module.exports = function () {
  this.lastScrapeContext = {};
  var self = this;
  this.setLastScrapeContext = function (context) {
    self.lastScrapeContext = context;
  };
  this.execute = function (donescraping) {
    if (self.lastScrapeContext === null) {
      self.lastScrapeContext = {};
    }
    scrapePage(bukopinPromotionBaseUrl)
      .then(function (promotions) {
        donescraping(null, promotions, self.lastScrapeContext);
      });
  };
  function requestWithPromise(address, method) {
    console.log('requesting %s', address);
    return requestPromise({
      method: method,
      url: address,
      timeout: requestTimeout
    }).then(function (body) {
      console.log('[success] request: %s', address);
      return Promise.resolve(body[1]);
    }).catch(function (err) {
      if (err.code === 'ESOCKETTIMEDOUT') {
        console.error('[failed] request timed out: %s', address);
        return Promise.resolve(null);
      } else {
        console.error('[failed] request: %s', address);
        console.error(err);
        return Promise.reject(err);
      }
    });
  }

  function scrapePage(url) {
    return requestWithPromise(url, 'GET')
      .then(function (body) {
        if (body) {
          var promotions = findTagThatContainPromo(body);
          promotions = getPromoData(promotions);
          self.lastScrapeContext = setCurrentScrapeContext(promotions);
          return promotions;
        } else {
          return [];
        }
      });
  }

  function findTagThatContainPromo(body) {
    var $ = cheerio.load(body);
    var $scriptTags = $('script');
    var tagDataThatContainJsonString = '';
    _.forEach($scriptTags, function (cur) {
      if (isTagContainJsonString(cur)) {
        var data = _.split(cur.children[0].data, 'window._sharedData = ');
        tagDataThatContainJsonString = data[1].slice(0, -1);
      }
    });
    var promotions = JSON.parse(tagDataThatContainJsonString).entry_data.ProfilePage[0].user.media.nodes;
    return promotions;
  }

  function isTagContainJsonString(currentTag) {
    return (currentTag.hasOwnProperty('children') && Array.isArray(currentTag.children) &&
    currentTag.children.length > 0 && currentTag.children[0].hasOwnProperty('data') &&
    _.startsWith(currentTag.children[0].data, 'window._sharedData'));
  }

  function getPromoData (promotions) {
    var continueScraping = true;
    return _.reduce(promotions, function (retPromos, currentPromo) {
      if (!continueScraping) {
        return retPromos;
      } else {
        var promoObj = {};
        promoObj.name = 'Promo Bukopin ' + currentPromo.code;
        promoObj.sourceUrl = bukopinPromotionBaseUrl;
        promoObj.description = replaceBackSlashN(currentPromo.caption);
        promoObj.imageUrl = currentPromo.display_src;
        if (_.isEqual(promoObj, self.lastScrapeContext)) {
          continueScraping = false;
        } else {
          retPromos.push(promoObj);
        }
        return retPromos;
      }
    }, []);
  }

  function setCurrentScrapeContext(promotions) {
    if (promotions.length === 0) {
      return self.lastScrapeContext;
    } else {
      return promotions[0];
    }
  }

  function replaceBackSlashN(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }
};
