'use strict';

var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));
var cheerio = require('cheerio');
var _ = require('lodash');

var bankMegaPromoBaseUrl =
  'https://www.bankmega.com/ajax.promolainnya.php?product=1&subcat={subcategoryNumber}&page={pageNumber}';
var bankMegaBaseUrl = 'https://www.bankmega.com';
var numberOfSubcategory = 5;
var categories = ['Travel & Entertainment', 'Lifestyle & Wellness', 'Food & Beverage', 'Gadget & Electorinics',
                  'Daily need & Home Appliances'];
var requestTimeout = 60000;
var bankName = 'Bank Mega';

module.exports = function () {
  this.setLastScrapeContext = function (context) {
    // Since scraper need to scrape all data,
    // then do nothing
  };
  this.execute = function (doneScraping) {
    scrape()
      .then(function (promotions) {
        promotions = _.flattenDeep(promotions);
        promotions = _.compact(promotions);
        doneScraping(null, promotions, null);
      });
  };
  function requestWithPromise(opt) {
    console.log('requesting %s', opt.url);
    return requestPromise(opt)
      .then(function (body) {
        console.log('[success] request: %s', opt.url);
        return Promise.resolve(body[1], null);
      }).catch(function (err) {
        if (err.code === 'ESOCKETTIMEDOUT') {
          console.error('[failed] request timed out: %s', opt.url);
          return Promise.resolve(null, err.code);
        } else {
          console.error('[failed] request: %s', opt.url);
          console.error(err, err.code);
          return Promise.resolve(null);
        }
      });
  }

  function scrape() {
    var scrapePageFunctionList = [];
    for (var subcategoryNumber = 1; subcategoryNumber <= numberOfSubcategory; subcategoryNumber++) {
      scrapePageFunctionList.push(scrapePage(subcategoryNumber, 1));
    }
    return Promise.all(scrapePageFunctionList);
  }

  function scrapePage(subcategoryNumber, pageNumber) {
    var url = _.replace(bankMegaPromoBaseUrl, '{subcategoryNumber}', subcategoryNumber);
    
    url = _.replace(url, '{pageNumber}', pageNumber);
    var requestOpt = {
      method: 'GET',
      url: url,
      timeout: requestTimeout,
      headers: {
        Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'
      }
    };
    return requestWithPromise(requestOpt)
      .then(function (body, errorCode) {
        if (!body) {
          if (errorCode === 'ESOCKETTIMEDOUT') {
            // Timeout, request to next page
            return scrapePage(subcategoryNumber, pageNumber + 1);
          }
          return [];
        }
        var $ = cheerio.load(body);
        var $anchorThatContainUrls = $('a', '.clearfix');
        var scrapeDetailPageFunctionList = [];
        if ($anchorThatContainUrls.length === 0) {
          return [];
        }
        //

        $anchorThatContainUrls = _.filter($anchorThatContainUrls, function (cur) {
          return cur.attribs.href.indexOf('promo_detail.php') > -1;
        });
        _.forEach($anchorThatContainUrls, function (cur) {
          scrapeDetailPageFunctionList.push(scrapeDetailPage(bankMegaBaseUrl + '/' + cur.attribs.href, subcategoryNumber));
        });
        var closure = {};
        return Promise.all(scrapeDetailPageFunctionList)
          .then(function (result) {
            closure.result = result;
            return scrapePage(subcategoryNumber, pageNumber + 1);
          })
          .then(function (res) {
            closure.result = _.concat(closure.result, res);
            return closure.result;
          });
      });
  }

  function scrapeDetailPage(url, subcategoryNumber) {
    var requestOpt = {
      method: 'GET',
      url: url,
      timeout: requestTimeout,
      headers: {
        Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'
      }
    };
    return requestWithPromise(requestOpt)
      .then(function (body) {
        if (!body) {
          return null;
        }
        var promoObj = {};
        var $ = cheerio.load(body);
        promoObj.sourceUrl = url;
        promoObj.name = getPromotionName($) + ' [' + bankName + ']';
        promoObj.description = getPromotionDescription($);
        promoObj.imageUrl = getPromotionImageUrl($);
        promoObj.category = categories[subcategoryNumber - 1];
        return promoObj;
      });
  }

  function getPromotionName($) {
    return replaceBackSlashNWith($('.titleinside').text(), '');
  }

  function getPromotionDescription($) {
    return replaceBackSlashNWith($('.area').text(), '<br />');
  }

  function getPromotionImageUrl($) {
    return bankMegaBaseUrl + $('img', '.keteranganinside').attr('src');
  }

  function replaceBackSlashNWith(str, replacement) {
    str = str.replace(/(?:\t)/g, '');
    return str.replace(/(?:\r\n|\r|\n)/g, replacement);
  }
};
