/**
 * This script is used to listen and poll messages
 * from amazon sqs queue and execute the command.
 * @author Dewangga <dewangga@cermati.com>
 * @param queueName
 * usage example: node receiver.js --queueName <queue name>
 */
'use strict';

var Bluebird = require('bluebird');
var scrapingHelper = require('../../utils/scrapingHelper');
var logger = require('@cermati/cermati-utils/logger')(__filename);
var exec = require('child-process-promise').exec;
var AWS = require('aws-sdk');
var _ = require('lodash');
var promiseHelper = require('../../modules/common/helpers/promise');
var argv = require('yargs').argv;
var Psaux = require('ps-aux');
var psaux = Bluebird.promisifyAll(Psaux());

// Required so the child process can read /etc/environment variables
require('dotenv').config({path: '/etc/environment'});

scrapingHelper.loadEnv();

var initialSleepTime = 1;
var sleepTime = initialSleepTime;
var maxSleepTime = 2 * 60 * 60;
var accessKey = process.env.AMAZON_ACCESS_KEY_ID;
var secretKey = process.env.AMAZON_SECRET_ACCESS_KEY;
var awsRegion = process.env.AMAZON_SQS_REGION;
var queueName = process.env.ASYNC_TASK_QUEUE_NAME;
var queueUrl = process.env.AMAZON_SQS_QUEUE_URL + queueName;
var batchSize = 10;
var waitTimeSeconds = 20;
var visibilityTimeout = 5 * 60;
var processTimeout = 60 * 60 * 1000;

AWS.config.setPromisesDependency(Bluebird);

AWS.config.update({
  region: awsRegion,
  accessKeyId: accessKey,
  secretAccessKey: secretKey
});

var sqs = new AWS.SQS({
  apiVersion: '2012-11-05',
  region: awsRegion
});



/**
 * The workflow of this script is:
 * 1. Start long polling to receive some messages.
 * 2. Delete messages and execute all command in message body synchronously.
 *    2.1. If execution timed out, kill the child process
 * 3. Sleep for sleep time seconds.
 * 4. If there is no message received after long polling,
 *    multiply sleep time by two.
 * 5. Start the start() method again
 * @returns {Bluebird}
 */
var start = function () {
  if (argv.queueName) {
    queueName = argv.queueName;
    queueUrl = process.env.AMAZON_SQS_QUEUE_URL + queueName;
  }
  return execute()
    .then(function () {
      var oldSleepTime = sleepTime;
      if (sleepTime * 2 <= maxSleepTime) {
        sleepTime *= 2;
      }
      logger.info('sleep for %d seconds', oldSleepTime);
      return Bluebird.resolve().delay(oldSleepTime * 1000);
    });
};

function execute() {
  return pollMessage()
    .then(function (messages) {
      if (_.isEmpty(messages)) {
        logger.info('got 0 message');
        return Bluebird.resolve();
      }
      logger.info('got %d message/s from queue', messages.length);
      messages.sort(function (a, b) {
        return JSON.parse(a.Body).priority - JSON.parse(b.Body).priority;
      });
      sleepTime = initialSleepTime;
      return executeCommands(messages, 0);
    });
}

function pollMessage() {
  return new Bluebird(function (resolve) {
    var receiveParam = {
      QueueUrl: queueUrl,
      MaxNumberOfMessages: batchSize,
      WaitTimeSeconds: waitTimeSeconds,
      VisibilityTimeout: visibilityTimeout
    };
    logger.info('start polling');
    var sqsReceiveMessagePromise = sqs.receiveMessage(receiveParam).promise();
    sqsReceiveMessagePromise
      .then(function (data) {
        logger.info('finish polling');
        resolve(data.Messages);
      })
      .catch(function (err) {
        logger.info(err);
        resolve(null);
      });
  });
}

/**
 * This function never reject if an error happen,
 * it will only log the error.
 * @param message
 * @returns {bluebird|exports|module.exports}
 */
function deleteMessage(message) {
  return new Bluebird(function (resolve) {
    if (message === null) {
      resolve();
    }
    var deleteParams = {
      QueueUrl: queueUrl,
      ReceiptHandle: message.ReceiptHandle
    };
    logger.info('deleting message');
    sqs.deleteMessage(deleteParams, function (err) {
      if (err) {
        logger.info('error deleting message');
        logger.error(err);
        logger.info(err.stack);
      } else {
        logger.info('message removed');
      }
      resolve();
    });
  });
}

/**
 * This function used to extract command to get
 * the base command.
 * base command is the last command after a series of &&
 * example:
 *    command: cd /some/path && node something.js >> something.log 2>&1
 *    baseCommmand: node something.js
 * @param {String} command
 * @returns {String}
 */
function getBaseCommand(command) {
  var splittedCommands = _.split(command, '&&', 5);
  var baseCommand = splittedCommands[splittedCommands.length - 1];
  baseCommand = _.split(baseCommand, />>/, 5)[0];
  baseCommand = _.trim(baseCommand);
  return baseCommand;
}

function killCommand(pid) {
  logger.info('executing kill -9 %d', pid);
  return exec('kill -9 ' + pid);
}

function killProcess(command) {
  // Get the base command
  var baseCommand = getBaseCommand(command);

  // First, do ps aux
  return psaux.parsedAsync()
    .then(function (res) {
      // Filter ps aux result by baseCommand
      var procs = _.filter(res, function (proc) {
        return _.isEqual(proc.command, baseCommand);
      });

      // In case more than one process with command equal to baseCommand,
      // kill all the process.
      var killCommandList = [];
      _.forEach(procs, function (proc) {
        var pid = proc.pid;
        logger.info('kill %s process with pid = %d', baseCommand, pid);
        killCommandList.push(killCommand(pid));
      });
      return Bluebird.all(killCommandList);
    })
    .then(function () {
      logger.info('finish killing');
    })
    .catch(function (err) {
      logger.error(err);
    });
}

var runner = promiseHelper.runner(start);

// Use promiseHelper to loop proccess infinitely
runner.while(_.constant(true)).catch(logger.error);
function executeCommands(messages, idx) {
  var command = JSON.parse(messages[idx].Body).body;
  if (JSON.parse(messages[idx].Body).timeout) {
    processTimeout = JSON.parse(messages[idx].Body).timeout;
  }

  // First, delete the message
  return deleteMessage(messages[idx])
    .then(function () {
      logger.info('executing %s', command);
      logger.info('timeout = %d ms', JSON.parse(messages[idx].Body).timeout);

      // Executing command
      var childProcessPromise = exec(command);
      return promiseHelper.wrapWithBluebird(childProcessPromise);
    })

    // Set process timeout
    .timeout(JSON.parse(messages[idx].Body).timeout)
    .then(function (result) {
      logger.info('Finish executing [%s]', command);

      // Execute next command
      if (idx + 1 >= messages.length) {
        Bluebird.resolve();
      } else {
        return executeCommands(messages, idx + 1);
      }
    })

    // If process timed out, kill the process
    // then delete the message
    .catch(Bluebird.TimeoutError, function (err) {
      logger.error('[Timeout] when executing %s', command);
      logger.error(err);
      return killProcess(command);
    })

    // If error happen while executing command,
    // just delete the message from queue
    .catch(function (err) {
      logger.error('error executing %s', command);
      logger.error(err);

      // Execute next command
      if (idx + 1 >= messages.length) {
        Bluebird.resolve();
      } else {
        return executeCommands(messages, idx + 1);
      }
    });
}
