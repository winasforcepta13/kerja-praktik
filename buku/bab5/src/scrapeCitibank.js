'use strict';

var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var _ = require('lodash');
var cloudinary = require('cloudinary');
var url = require('url');
var he = require('he');
var utils = require('../../utils/scrapingHelper');

var creditCardPromoBaseUrl = 'http://www.citibank.co.id/bahasa/static/penawaran-kartu-kredit.htm';
var citibankBaseUrl = 'http://www.citibank.co.id';
var requestTimeout = 10000;
var maxBannerTextLenght = 30;
var bankName = 'Citibank';

module.exports = function () {
  this.setLastScrapeContext = function (context) {
    // Not implemented yet
  };
  function getScraperName() {
    return 'citibank';
  }
  this.execute = function (doneScraping) {
    utils.setCloudinaryConfig(cloudinary);
    requestWithPromise(creditCardPromoBaseUrl, 'GET')
      .then(function (body) {
        if (body) {
          var categories = getCategory(body);
          var promotions = getPromotionLinks(body, categories);
          return getPromotionDetail(promotions);
        }
      })
      .then(function (promotionObject) {
        return transformPromotionImages(promotionObject);
      })
      .then(function (promotions) {
        doneScraping(null, promotions, null);
      })
      .catch(function (err) {
        console.error(err);
      });
  };
  function getCategory(body) {
    var $ = cheerio.load(body);
    var $categoryAnchors = $('a', '.cardsTabHldr');
    var categories = _.reduce($categoryAnchors, function (tempCategories, currentAnchor) {
      if (currentAnchor.attribs.title !== 'All Promo') {
        var categoryObject = {};
        categoryObject.name = currentAnchor.attribs.title;
        categoryObject.id = _.replace(_.snakeCase(currentAnchor.attribs.title), '_', '');
        tempCategories.push(categoryObject);
      }
      return tempCategories;
    }, []);
    return categories;
  }

  function requestWithPromise(address, method) {
    console.log('requesting %s', address);
    return requestPromise({
      method: method,
      url: address,
      timeout: requestTimeout
    }).then(function (body) {
      console.log('[success] request: %s', address);
      return Promise.resolve(body[1]);
    }).catch(function (err) {
      if (err.code === 'ESOCKETTIMEDOUT') {
        console.error('[failed] request timed out: %s', address);
        return Promise.resolve(null);
      } else {
        console.error('[failed] request: %s', address);
        console.error(err);
        return Promise.reject(err);
      }
    });
  }

  function getPromotionLinks(body, categories) {
    var $ = cheerio.load(body);
    var promotions = [];
    _.forEach(categories, function (category) {
      var $pageLinks = $('a', '#' + category.id);
      promotions = _.concat(promotions, _.reduce($pageLinks, function (theObjects, now) {
        var theObject = {};
        theObject.scraperName = getScraperName();
        theObject.url = citibankBaseUrl + now.attribs.href;
        theObject.category = category.name;
        theObjects.push(theObject);
        return theObjects;
      }, []));
    });
    return promotions;
  }

  function getPromotionDetail(promotions) {
    return new Promise(function (resolve, reject) {
      var detailPageFunctionList = [];
      _.forEach(promotions, function (currentPromotion) {
        detailPageFunctionList.push(getPageDetail(currentPromotion));
      });
      Promise.all(detailPageFunctionList)
        .then(function (promotionObject) {
          // Remove null object from promotionObject
          _.remove(promotionObject, function (current) {
            return (current === null);
          });
          resolve(promotionObject);
        })
        .catch(function (err) {
          console.error(err);
        });
    });
  }

  function getPageDetail(promotion) {
    return new Promise(function (resolve, reject) {
      requestWithPromise(promotion.url, 'GET')
        .then(function (body) {
          if (!body) {
            resolve(null);
          } else {
            var $ = cheerio.load(body);
            promotion.name = getPromotionName(body) + ' [' + bankName + ']';
            promotion.sourceUrl = promotion.url;
            var tabHead = getTabHead(body);
            if (tabHead.length === 0) {
              resolve(null);
            }
            var tabContent = getTabContent(body);
            var syaratDanKetentuan = getSyaratDanKetentuan(body);
            var pageDetail = zipHeadAndContent(tabHead, tabContent, syaratDanKetentuan);
            var imageUrl = citibankBaseUrl + $('.fullBanner').attr('src');
            promotion.description = pageDetail;
            promotion.imageUrl = imageUrl;
            promotion.imageText = getImageText(body);
            resolve(promotion);
          }
        })
        .catch(function (err) {
          resolve(null);
        });
    });
  }

  function getTabHead(body) {
    var $ = cheerio.load(body);
    var $tabHead = $('.tabHeading');
    $tabHead = _.reduce($tabHead, function (retArray, current) {
      if (current.children[0].data) {
        retArray.push(current.children[0].data);
      }
      return retArray;
    }, []);
    return $tabHead;
  }

  function getTabContent(body) {
    var $ = cheerio.load(body);
    var $tabContent = $('.tabContent');
    $tabContent = _.map($tabContent, function (current) {
      current = $(current).html();
      return current;
    });
    return $tabContent;
  }

  function getSyaratDanKetentuan(body) {
    var $ = cheerio.load(body);
    var $syaratDanKetentuan = $('.TermsAndConditions');
    if (!$syaratDanKetentuan) {
      return null;
    }
    return $($syaratDanKetentuan).html();
  }

  function zipHeadAndContent(tabHead, tabContent, syaratDanKetentuan) {
    // Some pages has empty tab (the tab redirect to another page)
    tabContent = _.slice(tabContent, 0, tabHead.length);
    if (syaratDanKetentuan) {
      tabHead.push('Syarat dan Ketentuan');
      tabContent.push(syaratDanKetentuan);
    }
    var details = _.zip(tabHead, tabContent);
    var pageDetail = _.reduce(details, function (detail, current) {
      detail += '<h3>' + current[0] + '</h3>';
      detail += current[1];
      return detail;
    }, '');
    return pageDetail;
  }

  function getPromotionName(body) {
    var $ = cheerio.load(body);
    return _.trim($('h1').text());
  }

  function getImageText(body) {
    var $ = cheerio.load(body);
    var str = $('.cH-bannerRhtHdr').html();
    str = reformatText(str);
    return str;
  }

  function reformatText(str) {
    str = he.decode(str);
    str = str.replace('</p>', '\n');
    str = str.replace(/<[^>]*>/gm, '');
    str = _.trim(str);
    str = splitTextIntoSeveralLines(str);
    str = _.trim(str);
    str = encodeURIComponent(str);
    str = str.replace(/(%0A%20)+/gm, '%0A');
    str = str.replace(/(%0A)+/gm, '%0A');
    return str;
  }

  function splitTextIntoSeveralLines(str) {
    str = encodeURIComponent(str);
    str = str.replace(/(%0D|%09|%0B)+/gm, '');
    str = str.replace(/(%20)+/gm, '%20');
    str = str.replace(/(%0A)+/gm, '%0A');
    str = decodeURIComponent(str);
    var splitedStr = _.split(str, ' ');
    var strLengthCount = 0;
    str = _.reduce(splitedStr, function (retStr, cur) {
      if (cur.indexOf('\n') > -1) {
        var splitedCurStr = _.split(cur, /\n/);
        if (strLengthCount + splitedCurStr[0].length + 1 <= maxBannerTextLenght) {
          retStr += splitedCurStr[0] + '\n';
          strLengthCount = 0;
          retStr += splitedCurStr[1] + ' ';
          strLengthCount += splitedCurStr[1].length + 1;
        } else {
          retStr += '\n' + splitedCurStr[0] + '\n';
          strLengthCount = 0;
          retStr += splitedCurStr[1] + ' ';
          strLengthCount += splitedCurStr[1].length + 1;
        }
      } else {
        if (strLengthCount + cur.length + 1 <= maxBannerTextLenght) {
          retStr += cur + ' ';
          strLengthCount += cur.length + 1;
        } else {
          retStr += '\n';
          strLengthCount = 0;
          retStr += cur + ' ';
          strLengthCount += cur.length + 1;
        }
      }
      return retStr;
    }, '');
    return str;
  }

  function transformPromotionImages(promotions) {
    return new Promise(function (resolve, reject) {
      var transformImageFunctionList = [];
      _.forEach(promotions, function (cur) {
        transformImageFunctionList.push(transformImage(cur));
      });
      Promise.all(transformImageFunctionList)
        .then(function (result) {
          resolve(result);
        });
    });
  }

  function transformImage(promotion) {
    return new Promise(function (resolve, reject) {
      var uploadOpt = {
        folder: 'promotion',
        timeout: 60000,
        types: ['promotion', 'imageTest']
      };

      cloudinary.uploader.upload(promotion.imageUrl, function (result) {
        var imageUrl = result.url;
        var overlayTextUrlParameter = getOverlayTextUrlParameter(promotion.imageText);
        promotion.imageUrl = addOverlayTextToImage(imageUrl, overlayTextUrlParameter);
        resolve(promotion);
      }, uploadOpt);
    });
  }

  function getOverlayTextUrlParameter(text) {
    return 'l_text:Arial_25:' + text + ',g_north_east,y_50,x_30,co_rgb:FFFFFF';
  }

  function addOverlayTextToImage(url, overlayTextUrlParameter) {
    var splitedUrl = _.split(url, '/');
    var transformedUrl = _.reduce(splitedUrl, function (retUrl, cur) {
      if (!(cur === 'http:' || cur === 'https:')) {
        retUrl += '/';
      }
      retUrl += cur;
      if (cur === 'upload') {
        retUrl += '/' + overlayTextUrlParameter;
      }
      return retUrl;
    }, '');
    return transformedUrl;
  }
};
