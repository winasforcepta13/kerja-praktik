'use strict';

var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));
var request = require('request');
var cheerio = require('cheerio');
var _ = require('lodash');

var creditCardPromoBaseUrl = 'https://www.danamonline.com/onlinebanking/include/id/popups/';
var normalPromotionPageIndex = [2, 3, 4, 5, 6, 7, 9];
var mileagePageIndex = 10;
var rewarPointsPageIndex = 8;
var requestTimeout = 10000;
var bankName = 'Danamon';

module.exports = function () {
  this.setLastScrapeContext = function (context) {
    // Not implemented yet
  };
  function getScraperName() {
    return 'danamon';
  }
  this.execute = function (doneScraping) {
    scrape()
      .then(function (promotionResult) {
        var promotions = [];
        _.forEach(promotionResult, function (current) {
          promotions = _.concat(promotions, current);
        });
        doneScraping(null, promotions, null);
      })
      .catch(doneScraping);
  };
  function requestWithPromise(address, method) {
    console.log('requesting %s', address);
    return requestPromise({
      method: method,
      url: address,
      timeout: requestTimeout
    }).then(function (body) {
      console.log('[success] request: %s', address);
      return Promise.resolve(body[1]);
    }).catch(function (err) {
      if (err.code === 'ESOCKETTIMEDOUT') {
        console.error('[failed] request timed out: %s', address);
        return Promise.resolve(null);
      } else {
        console.error('[failed] request: %s', address);
        console.error(err);
        return Promise.reject(err);
      }
    });
  }

  function scrape() {
    return new Promise(function (resolve, reject) {
      var promolocationFunctionList = [];
      _.forEach(normalPromotionPageIndex, function (current) {
        var promoUrl = creditCardPromoBaseUrl + 'nlvimas_';
        promoUrl += '0';
        promoUrl += current + '.html';
        promolocationFunctionList.push(scrapePromo(promoUrl));
      });
      Promise.all(promolocationFunctionList)
        .then(function (promotionObject) {
          return scrapeMilagePage(promotionObject);
        })
        .then(function (promotions) {
          return scrapeRewardPointsPage(promotions);
        })
        .then(function (promotions) {
          return resolve(promotions);
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  }

  function scrapePromo(url) {
    return new Promise(function (resolve, reject) {
      requestWithPromise(url, 'GET')
        .then(function (body) {
          if (!body) {
            reject('[failed] scraping ' + url);
          }
          var $ = cheerio.load(body);
          var location = getlocationName($);
          return getAllPromotionInPage(location, $, url);
        })
        .then(function (promotionsResult) {
          return resolve(promotionsResult);
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  }

  function getlocationName($) {
    return $('.selected').text();
  }

  function getAllPromotionInPage(location, $, url) {
    return new Promise(function (resolve, reject) {
      var getPromoFunctionList = [];
      var $promos = $('.even');
      _.forEach($promos, function (promo) {
        getPromoFunctionList.push(getPromo(promo, location, url));
      });
      Promise.all(getPromoFunctionList)
        .then(function (result) {
          return resolve(result);
        })
        .catch(function (err) {
          console.log(err);
        });
    });
  }

  function getPromo(promo, location, url) {
    return new Promise(function (resolve, reject) {
      var promoObject = {};
      promoObject.name = getPromoName(promo) + ' [' + bankName + ']';
      promoObject.imageUrl = getPromoImageUrl(promo);
      promoObject.description = getPromoDescription(promo);
      promoObject.description += '<p> <strong> ' + location + '</strong> </p>';
      promoObject.sourceUrl = url;
      resolve(promoObject);
    });
  }

  function getPromoName(promo) {
    var $ = cheerio.load(promo);
    var promoName = $('.cr-title').text();
    return _.trim(promoName);
  }

  function getPromoImageUrl(promo) {
    var $ = cheerio.load(promo);
    var promoImageUrl = $('img', '.cleft').attr('src');
    return creditCardPromoBaseUrl + promoImageUrl;
  }

  function getPromoDescription(promo) {
    var $ = cheerio.load(promo);
    var promoDescription = $('.cr-content', '.cright').html();
    return promoDescription;
  }

  function scrapeMilagePage(prevPromotions) {
    return new Promise(function (resolve, reject) {
      var mileageUrl = creditCardPromoBaseUrl + 'nlvimas_' + mileagePageIndex + '.html';
      requestWithPromise(mileageUrl, 'GET')
        .then(function (body) {
          if (!body) {
            reject('[failed] scraping ' + mileageUrl);
          }
          var mileagePromo = getMileagePromo(mileageUrl, body);
          prevPromotions = _.concat(prevPromotions, mileagePromo);
          resolve(prevPromotions);
        });
    });
  }

  function getMileagePromo(url, body) {
    var promoObject = {};
    var $ = cheerio.load(body);
    promoObject.name = 'Mileage';
    promoObject.sourceUrl = url;
    promoObject.imageUrl = creditCardPromoBaseUrl + $('img', '.table-responsive').attr('src');
    promoObject.description = $('.table-responsive').html();
    return promoObject;
  }

  function scrapeRewardPointsPage(prevPromotions) {
    return new Promise(function (resolve, reject) {
      var rewardPointsPageUrl = creditCardPromoBaseUrl + 'nlvimas_0' + rewarPointsPageIndex + '.html';
      requestWithPromise(rewardPointsPageUrl, 'GET')
        .then(function (body) {
          if (!body) {
            reject('[failed] scraping ' + rewardPointsPageUrl);
          }
          var $ = cheerio.load(body);
          var $promos = $('.even');
          var getPromoFunctionList = [];
          _.forEach($promos, function (current) {
            $ = cheerio.load(current);
            if ($('img', '.cleft').attr('src')) {
              getPromoFunctionList.push(getRewardPointsPromo(current, rewardPointsPageUrl));
            }
          });
          Promise.all(getPromoFunctionList)
            .then(function (res) {
              prevPromotions = _.concat(prevPromotions, res);
              resolve(prevPromotions);
            })
            .catch(function (err) {
              console.log(err);
            });
        });
    });
  }

  function getRewardPointsPromo(promo, url) {
    return new Promise(function (resolve, reject) {
      var promoObject = {};
      var $ = cheerio.load(promo);
      promoObject.imageUrl = creditCardPromoBaseUrl + $('img', '.cleft').attr('src');
      if ($('.separator').text()) {
        promoObject.name = $('.separator').text();
      } else {
        promoObject.name = 'Penawaran Khusus';
      }
      promoObject.sourceUrl = url;
      promoObject.description = $('.cr-content').html();
      resolve(promoObject);
    });
  }
};
