/**
 * This script is used to send asynchronous task to amazon sqs
 * to avoid race condition while accessing the database.
 * @param command (mandatory)
 * @param queueName
 * @param priority
 * @param timeout
 * usage example: node sender.js --queueName <queue name> --command '<command>' --priority <priority>
 *                --timeout <timeout in milisecond>
 *
 */
'use strict';

var Promise = require('bluebird');
var sqs = require('easy-sqs');
var uuid = require('uuid');
var scrapingHelper = require('../../utils/scrapingHelper');
var _ = require('lodash');
var logger = require('@cermati/cermati-utils/logger')(__filename);
var argv = require('yargs').argv;

scrapingHelper.loadEnv();

var accessKey = process.env.AMAZON_ACCESS_KEY_ID;
var secretKey = process.env.AMAZON_SECRET_ACCESS_KEY;
var awsRegion = process.env.AMAZON_SQS_REGION;
var queueName = process.env.ASYNC_TASK_QUEUE_NAME;
var queueUrl = process.env.AMAZON_SQS_QUEUE_URL + queueName;
var maxPushTry = 5;
var processTimeout = 60 * 60 * 1000;
/**
 * Priority is a number that indicate the priority of a task (1 - 10).
 * The smaller one will be executed first.
 * If priority isn't passed as argument, it will be set by 10.
 * @type {number}
 */
var priority = 10;

var awsConfig = {
  accessKeyId: accessKey,
  secretAccessKey: secretKey,
  region: awsRegion
};

var client = sqs.createClient(awsConfig);

/**
 * This function is used to execute the main process
 */
function execute() {
  var command = argv.command;
  if (argv.queueName) {
    queueName = argv.queueName;
    queueUrl = process.env.AMAZON_SQS_QUEUE_URL + queueName;
  }
  if (argv.priority) {
    priority = argv.priority;
  }
  if (argv.timeout) {
    processTimeout = argv.timeout;
  }
  logger.info('push command to %s', queueName);
  client.getQueue(queueUrl, function (err, queue) {
    if (err) {
      logger.error('queue [%s] doesn\'t exist', queueName);
    } else {
      pushMessage(queue, command, priority)
        .then(function () {
          logger.info('command [%s] pushed to queue [%s]', command, queueName);
        })
        .catch(function (err) {
          logger.error(err);
        });
    }
  });
}

/**
 * This function is used to push message to queue
 * @param message
 * @returns {bluebird|exports|module.exports}
 */
function pushMessage(queue, message, numberOfTry) {
  return new Promise(function (resolve, reject) {
    var msg = JSON.stringify({
      body: message,
      id: uuid.v4(),
      priority: priority,
      timeout: processTimeout
    });
    queue.sendMessage(msg, function (err) {
      if (err) {
        if (numberOfTry < maxPushTry) {
          logger.info('error pushing message [%s] to queue [%s]', message, queueName);
          logger.info('retry push [%s] to [%s]', message, queueName);
          pushMessage(queue, message, numberOfTry + 1)
            .catch(function (err) {
              reject(err);
            });
        } else {
          reject('failed pushing message [%s] to queue [%s]', message, queueName);
        }
      }
      resolve();
    });
  });
}

execute();
