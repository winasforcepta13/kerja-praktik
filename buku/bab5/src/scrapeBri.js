'use strict';


var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));
var cheerio = require('cheerio');
var _ = require('lodash');

var briPromotionBaseUrl = 'http://kartukredit.bri.co.id/promotion?sorting=newest&page=';
var requestTimeout = 60000;
var bankName = 'BRI';

module.exports = function () {
  this.lastScrapeContext = {};
  this.continueScraping = true;
  var self = this;
  this.setLastScrapeContext = function (context) {
    self.lastScrapeContext = context;
  };
  this.execute = function (donescraping) {
    if (!self.lastScrapeContext) {
      self.lastScrapeContext = [];
    }
    scrapePromoPage(1, [])
      .then(function (promotions) {
        promotions = _.flattenDeep(promotions);
        self.lastScrapeContext = _.concat(promotions, self.lastScrapeContext);
        donescraping(null, promotions, self.lastScrapeContext);
      });
  };
  function requestWithPromise(address, method) {
    console.log('requesting %s', address);
    return requestPromise({
      method: method,
      url: address,
      timeout: requestTimeout
    }).then(function (body) {
      console.log('[success] request: %s', address);
      return Promise.resolve(body[1]);
    }).catch(function (err) {
      if (err.code === 'ESOCKETTIMEDOUT') {
        console.error('[failed] request timed out: %s', address);
        return Promise.resolve(null);
      } else {
        console.error('[failed] request: %s', address);
        console.error(err);
        return Promise.reject(err);
      }
    });
  }

  function scrapePromoPage(pageNumber, promotions) {
    return getPagePromo(briPromotionBaseUrl + pageNumber)
      .then(function (result) {
        result = _.compact(result);
        if (result.length > 0) {
          promotions = _.concat(promotions, result);
          if (!self.continueScraping) {
            return promotions;
          } else {
            return scrapePromoPage(pageNumber + 1, promotions);
          }
        } else {
          return promotions;
        }
      });
  }

  function getPagePromo(url) {
    return new Promise(function (resolve, reject) {
      if (self.continueScraping) {
        requestWithPromise(url, 'GET')
          .then(function (body) {
            if (!body) {
              resolve(null);
            }
            var promotionUrls = getPromotionUrls(body);
            var scrapePromotionFunctionList = [];
            _.forEach(promotionUrls, function (promotionUrl) {
              if (self.continueScraping) {
                var urlIndexInLastRunContext = _.findIndex(self.lastScrapeContext, function (cur) {
                  return cur.sourceUrl === promotionUrl;
                });
                if (urlIndexInLastRunContext === -1) {
                  scrapePromotionFunctionList.push(scrapePromotion(promotionUrl));
                } else {
                  self.continueScraping = false;
                }
              }
            });
            Promise.all(scrapePromotionFunctionList)
              .then(function (result) {
                resolve(result);
              });
          });
      } else {
        resolve(null);
      }
    });
  }

  function getPromotionUrls(body) {
    var promotionUrls = [];
    var $ = cheerio.load(body);
    var $anchorObjects = $('a.btn-bri__navy', '.content-container');
    _.forEach($anchorObjects, function (current) {
      promotionUrls.push(current.attribs.href);
    });
    return promotionUrls;
  }

  function scrapePromotion(url) {
    return new Promise(function (resolve, reject) {
      requestWithPromise(url, 'GET')
        .then(function (body) {
          if (!body) {
            resolve(null);
          }
          var promotionObject = {};
          var $ = cheerio.load(body);
          promotionObject.sourceUrl = url;
          promotionObject.name = getPromotionName($) + ' [' + bankName + ']';
          promotionObject.description = getPromotionDescription($);
          promotionObject.imageUrl = getPromotionImageUrl($);
          promotionObject.startDate = getPromotionStartDate($);
          promotionObject.endDate = getPromotionEndDate($);
          resolve(promotionObject);
        });
    });
  }

  function getPromotionName($) {
    return $('h2', '.detail-item__tnc').text();
  }

  function getPromotionImageUrl($) {
    return $('img', '.promo-image-wrapper').attr('src');
  }

  function getPromotionDescription($) {
    return $('.promo-detail-table').html();
  }

  function getPromotionStartDate($) {
    var dateStr = $('p', '.detail-item__period').text();
    var splitedDateStr = _.split(dateStr, ' ');
    var retDate = splitedDateStr[0] + ' ' + getMonthInEnglish(splitedDateStr[1]) + ' ' + splitedDateStr[2];
    return new Date(retDate);
  }

  function getPromotionEndDate($) {
    var dateStr = $('p', '.detail-item__period').text();
    var splitedDateStr = _.split(dateStr, ' ');
    var retDate = splitedDateStr[4] + ' ' + getMonthInEnglish(splitedDateStr[5]) + ' ' + splitedDateStr[6];
    return new Date(retDate);
  }

  function getMonthInEnglish(month) {
    switch (month) {
      case 'Januari':
        return 'January';
      case 'Februari':
        return 'February';
      case 'Maret':
        return 'March';
      case 'April':
        return 'April';
      case 'Mei':
        return 'May';
      case 'Juni':
        return 'June';
      case 'Juli':
        return 'July';
      case 'Agustus':
        return 'August';
      case 'September':
        return 'September';
      case 'Oktober':
        return 'October';
      case 'November':
        return 'November';
      case 'Desember':
        return 'December';
    }
  }
};
